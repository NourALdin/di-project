import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.token,
    this.user,
  });

  String? token;
  UserItem? user;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    token: json["token"] == null ? null : json["token"],
    user: json["user"] == null ? null : UserItem.fromJson(json["user"]),
  );

  Map<String, dynamic> toJson() => {
    "token": token == null ? null : token,
    "user": user == null ? null : user?.toJson(),
  };
}

class UserItem {
  UserItem(
      {this.id,
        this.email,
        this.phoneNumber,
        this.type,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.username,
        this.engLvl,
        this.address,
        this.country,
        this.password,
        this.photoPathUrl,
        this.paymentIdentification,
        this.fullName,
        this.birthOfDate,
        // this.gender,
        this.specialId,
        this.referCode});

  int? id;
  String? email;
  String? phoneNumber;
  String? type;
  String? status;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? username;
  String? password;
  String? country;
  String? address;
  String? engLvl;
  var photoPathUrl;
  String? paymentIdentification;
  String? fullName;
  String? birthOfDate;

  // String gender;
  String? specialId;
  String? referCode;

  factory UserItem.fromJson(Map<String, dynamic> json) => UserItem(
    id: json["id"] == null ? null : json["id"],
    email: json["Email"] == null ? null : json["Email"],
    fullName: json["FullName"] == null ? null : json["FullName"],
    phoneNumber: json["PhoneNumber"] == null ? null : json["PhoneNumber"],
    referCode: json["Refer_Code"] == null ? null : json["Refer_Code"],
    type: json["type"] == null ? null : json["type"],
    status: json["status"] == null ? null : json["status"],
    username: json["username"] == null ? null : json["username"],
    password: json["password"] == null ? null : json["password"],
    country: json["Country"] == null ? null : json["Country"],
    address: json["Address"] == null ? null : json["Address"],
    engLvl: json["LvlEng"] == null ? null : json["LvlEng"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    photoPathUrl: json["photo_path_url"] == null ? null : json["photo_path_url"],
    paymentIdentification: json["PaymentIdentification"] == null ? null : json["PaymentIdentification"],
    birthOfDate: json["DateOfBirth"] == null ? null : json["DateOfBirth"],
    // gender: json["Gender"] == null ? null : json["Gender"],
    specialId: json["SpecialId"] == null ? null : json["SpecialId"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "Email": email == null ? null : email,
    "FullName": fullName == null ? null : fullName,
    "Refer_Code": referCode == null ? null : referCode,
    "type": type == null ? null : type,
    "status": status == null ? null : status,
    "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
    "username": username == null ? null : username,
    "password": password == null ? null : password,
    "Country": country == null ? null : country,
    "Address": address == null ? null : address,
    "LvlEng": engLvl == null ? null : engLvl,
    "PhoneNumber": phoneNumber == null ? null : phoneNumber,
    "photo_path_url": photoPathUrl == null ? null : photoPathUrl,
    "PaymentIdentification": paymentIdentification == null ? null : paymentIdentification,
    "DateOfBirth": birthOfDate == null ? null : birthOfDate,
    // "Gender": gender == null ? null : gender,
    "SpecialId": specialId == null ? null : specialId,
  };

  Map<String, dynamic> toJsonForMessages() => {
    "id": id == null ? null : id,
    "FullName": fullName == null ? null : fullName,
    "username": username == null ? null : username,
    "photo_path_url": photoPathUrl == null ? null : photoPathUrl,
  };
}
