// To parse this JSON data, do
//
//     final errorsModel = errorsModelFromJson(jsonString);

class ErrorsModel {
  ErrorsModel({
    this.result,
  });

  Result? result;

  factory ErrorsModel.fromJson(Map<String, dynamic> json) => ErrorsModel(
    result: json["Result"] == null ? null : Result.fromJson(json["Result"]),
  );

  Map<String, dynamic> toJson() => {
    "Result": result == null ? null : result?.toJson(),
  };
}

class Result {
  Result({
    this.statusCode,
    this.message,
  });

  int? statusCode;

  String? message;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    statusCode: json["statusCode"] == null ? null : json["statusCode"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "statusCode": statusCode == null ? null : statusCode,
    "message": message == null ? null : message,
  };
}
