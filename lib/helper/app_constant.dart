import 'dart:ui';

import 'package:flutter/material.dart';

class AppConstant {
  static BuildContext? context;
  static Size screenSize = const Size(0, 0);
  static const String apiBaseUrl = localUrl;

  static const String localUrl = "http://192.168.5.208:8000/api/";
}
