import 'package:di_app/di/auth_di.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
   const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}
class _LoginScreenState extends State<LoginScreen> with AuthDi{
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  @override
  void initState() {
    super.initState();
    initialization();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,

        children: [
          const Spacer(),
          TextFormField(
            controller: _emailController,
          ),
          const SizedBox(height: 40,),
          TextFormField(
            controller: _passwordController,
          ),
          const SizedBox(height: 40,),
          ElevatedButton(onPressed: (){
            authBloc.login(email: _emailController.value.text, password: _passwordController.value.text, onData: (v){});
          }, child:const Text("Login")),
          const Spacer(),
        ],
      ),
    );
  }
}