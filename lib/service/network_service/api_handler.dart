import 'dart:io';
import 'package:di_app/model/error_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class ApiHandler  {
  Map<String, String> headers = {
    'Content-Type': 'multipart/form-data',
    'Accept': 'application/json',
  };

  getMultiPartCallApi(url, context) async {
    ErrorsModel v;
    headers['version-os'] = Platform.isAndroid ? "android" : Platform.isIOS ? "ios" : "";
    // headers["version-number"] = dataStore.versionNumber;
    // print(url);
    // if (dataStore.user.token != null) {
    //   print(" is :Bearer  " + dataStore.user.token);
    //   headers['Authorization'] = "Bearer " + dataStore.user.token.toString();
    // } else {
    //   headers['Authorization'] = null;
    // }
    Dio dio =  Dio();
    var data;
    await dio
        .get(url,
        options: Options(
          headers: headers,
        ))
        .then((val) {
      print("______________________ response");
      print(val.data);
      data = val.data;
      try {
        switch (val.data['Result']['update_status']?.toString().toLowerCase() ?? "normal") {
          case "required":
            print("required");
            _updateWidget(context);
            throw Exception;
            break;
          case "optional":
            print("optional");
            // dataStore.getUpdateIsOption().then((value) {
            //   if (!value) {
            //     _updateWidget(context, isRequired: false);
            //     throw Exception;
            //   }
            // });
            break;
          default:
            print("default");
            break;
        }
      } catch (e) {}
    }).catchError((e) {
      print(e.response.statusCode);
      print(e.response.data);
      try {} catch (e) {}
      v = ErrorsModel.fromJson(e.response.data);
      // if (v.result.statusCode == 401) {
      //   dataStore.clearStoredData();
      //   Utils.instance.showToast(text: "you have login from another device please login to continue");
      //   Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => LoginScreen()), (Route<dynamic> route) => false);
      // }

      throw v;
    });
    return data;
  }

  postMultiPartCallApi(
      url,
      body,
      context,
      ) async {
    ErrorsModel v;
    print(url);
    print(body);
    print(headers);
    // Utils.instance.removeNullMapObjects(body);
    headers['version-os'] = Platform.isAndroid
        ? "android"
        : Platform.isIOS
        ? "ios"
        : "";
    // headers["version-number"] = dataStore.versionNumber;
    // print("version os is ${headers['version_os']}\nversion number is ${headers['version_number']}");
    // if (dataStore.user.token != null) {
    //   print("is :Bearer " + dataStore.user.token.toString());
    //   headers['Authorization'] = "Bearer " + dataStore.user.token.toString();
    // } else {
    //   headers['Authorization'] = null;
    // }

    Dio dio =  Dio();
    var data;
    await dio.post(url, data: body, options: Options(headers: headers)).then((val) {
      print("______________________ response");
      print(val.data);
      data = val.data;
      try {
        switch (val.data['Result']['update_status']?.toString().toLowerCase() ?? "normal") {
          case "required":
            print("required");
            _updateWidget(context);
            throw Exception;
            break;
          case "optional":
            print("optional");
            // dataStore.getUpdateIsOption().then((value) {
            //   if (!value) {
            //     _updateWidget(context, isRequired: false);
            //     throw Exception;
            //   }
            // });
            break;
          default:
            print("default");
            break;
        }
      } catch (e) {}
    }).catchError((e) {
      print("______________________ error");
      print(e.response.statusCode);

      print(e.response.data);

      print("!!!!!!!!!!!!!!!!!11");
      print(e);
      v = ErrorsModel.fromJson(e.response.data);
      // if (e.response.statusCode == 401) {
      //   dataStore.clearStoredData();
      //   Utils.instance.showToast(text: "you have login from another device please login to continue");
      //   Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => LoginScreen()), (Route<dynamic> route) => false);
      // }

      throw v;
    });
    return data;
  }


  _updateWidget(BuildContext context, {bool isRequired = true}) async {
    await showCupertinoDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) =>  AlertDialog(
          title:const Text(
            "Message",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
            textAlign: TextAlign.center,
          ),
          content:const  Text(
            "Writalk has a new update!\nEnjoy it now",
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: Center(
                child: Column(
                  children: [
                    FlatButton(
                      child:const Text(
                        'Update',
                        style: TextStyle(color: Colors.white, fontSize: 30),
                      ),
                      color: Colors.cyan,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                      onPressed: () {
                        //TODO rewrite code

                        /*Navigator.of(context).pop();

                        StoreRedirect.redirect(androidAppId: "com.writalk.writalk", iOSAppId: "1576062112").catchError((e) {
                          Utils.instance.showToast(text: "Store is not installed");
                        });*/
                      },
                    ),
                    !isRequired
                        ? Column(
                      children: [
                       const SizedBox(
                          height: 10,
                        ),
                        GestureDetector(
                          onTap: () {
                            //dataStore.setUpdateIsOption(true);
                            Navigator.of(context).pop();
                          },
                          child:const Text(
                            "Dismiss",
                            style: TextStyle(
                              color:Colors.blue,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        )
                      ],
                    )
                        : Container()
                  ],
                ),
              ),
            )
          ],
        ));
  }
}
