import 'package:di_app/model/user_model.dart';
import 'package:di_app/service/network_service/api_handler.dart';
import 'package:di_app/service/network_service/api_linke_container.dart';
import 'package:flutter/cupertino.dart';

class ApiProvider with ApiHandler{
  Future<UserModel?> login({required String email,required String password})async{
    Map body = {"Email":email,"Password":password};
    var data =await postMultiPartCallApi(ApiLinksContainer.loginURL, body,BuildContext);
    try{
      return UserModel.fromJson(data['Result']['data']);
    }catch(e){
      print(e);
    }
  }
}
