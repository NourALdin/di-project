import 'package:di_app/service/network_service/api_provider.dart';

class AuthBloc{
   final ApiProvider apiProvider;
   AuthBloc({required this.apiProvider});

   login({required String email,required String password,required Function onData}){
      apiProvider.login(email: email, password: password).then((value) {
         onData(value);
      }).catchError((e){
         print("login error $e");
      });
   }
}