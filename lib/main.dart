import 'package:di_app/di/di_container.dart';
import 'package:di_app/ui/auth/login_screen.dart';
import 'package:flutter/material.dart';

void main() {
  DiContainer().intiDiContainer();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: LoginScreen(),
    );
  }
}
