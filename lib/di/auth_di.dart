import 'package:di_app/bloc/auth_bloc.dart';
import 'package:get_it/get_it.dart';

abstract class AuthDi{
  final GetIt _getIt  = GetIt.instance;
  late AuthBloc authBloc;

  initialization(){
    authBloc = _getIt<AuthBloc>();
  }
}