import 'package:di_app/bloc/auth_bloc.dart';
import 'package:di_app/service/network_service/api_provider.dart';
import 'package:get_it/get_it.dart';

class DiContainer {
  final GetIt _getIt = GetIt.instance;

  void intiDiContainer() {
    _getIt.registerLazySingleton<ApiProvider>(() => ApiProvider());

    _getIt.registerFactory<AuthBloc>(() =>AuthBloc(apiProvider:_getIt<ApiProvider>()));
  }
}